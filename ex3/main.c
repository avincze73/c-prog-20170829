/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. augusztus 29., 13:11
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * 
 */
int accum(int* a, int length, int init)
{
    for (int i = 0; i < length; i++) {
        init += *(a + i);
    }
    return init;
}

int accum_2(int* begin, int* end, int init,
        int(*policy)(int,int),
        bool(*predicate)(int))
{
    for(;begin!=end;++begin)
    {
        if(predicate(*begin))
        {
            init = policy(init, *begin);
        }
    }
    return init;
}

int add(int a, int b)
{
    return a + b;
}

int mulitiply(int a, int b)
{
    return a * b;
}

bool positive(int a)
{
    return a > 0;
}

bool even(int a)
{
    return (a%2) == 0 ? true : false;
}
int main(int argc, char** argv) {

    int a[]={1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum(a, 10 , 0));
    printf("%d\n", accum_2(a, a+10, 0, add, even));
    return (EXIT_SUCCESS);
}


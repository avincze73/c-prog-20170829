/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. augusztus 29., 14:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/*
 * 
 */


int cstring_cmp(const void *a, const void *b) {
    return *((const char*) a) - *((const char*) b);
}

int is_anagram(const char* a, const char* b) {
    if (strlen(a) != strlen(b)) return 0;
    
    char* aa = (char*) malloc((strlen(a) + 1) * sizeof (char));
    strcpy(aa, a);
    qsort(aa, strlen(aa), sizeof (char), cstring_cmp);

    char* bb = (char*) malloc((strlen(b) + 1) * sizeof (char));
    strcpy(bb, b);
    qsort(bb, strlen(bb), sizeof (char), cstring_cmp);

    if (!strcmp(bb, aa)) return 1;
    return 0;
}

int main(int argc, char** argv) {

    bool b = is_anagram("abcdef", "bacdef");
    printf("%d", b);
    return (EXIT_SUCCESS);
}

